package com.cywc.mat6643.presenter;


import Jama.Matrix;
import edu.umbc.cs.maple.utils.JamaUtils;
import android.graphics.Bitmap;

import com.cywc.mat6643.ImageProcessActivity;
import com.cywc.mat6643.model.ImageModel;

public class ProcessManager {
	private ImageModel image;
	private ImageProcessActivity processor;
	
	public ProcessManager(ImageModel anImage, ImageProcessActivity aProcessor){
		image=anImage;
		processor=aProcessor;
	}
	
	public void flipImage(){
		Bitmap origin=ImageModel.bitmap;
		int width=origin.getWidth();
		int height=origin.getHeight();
		Bitmap flipped=Bitmap.createBitmap(width,height,Bitmap.Config.ARGB_8888);
		
		for(int i=0; i<width; i++){
			for(int j=0; j<height; j++){
				flipped.setPixel(i, j, origin.getPixel(width-1-i,height-1-j));
			}
		}
		
		
		
		processor.setImage(flipped);
		
	}
	
	public void showSVDSlides(int rank_max){
		Bitmap origin=ImageModel.bitmap;
		int width=origin.getWidth();
		int height=origin.getHeight();
		Bitmap canvas=Bitmap.createBitmap(width,height,Bitmap.Config.ARGB_8888);
		
		for(int i=0; i<width; i++){
			for(int j=0; j<height; j++){
				canvas.setPixel(i, j, 0);
			}
		}
		
		//processor.setImage(canvas);
		
		Matrix accumulative=new Matrix(width,height);
		for(int rank=1; rank<=rank_max; rank++){
			processor.setHintbar("Rank is " + rank);
			Matrix curr=new Matrix(width,height);
			
			Matrix Ui=JamaUtils.getcol(ImageModel.U, rank);
			Matrix Vi=JamaUtils.getcol(ImageModel.V, rank);
			curr=Ui.times(Vi.transpose());
			curr=curr.times(ImageModel.S[rank-1]);
			accumulative.plusEquals(curr);
		}
		
		for(int i=0; i<width; i++){
			for(int j=0; j<height; j++){
				canvas.setPixel(i, j, (int)accumulative.get(i,j));
			}
		}
		
		processor.setImage(canvas);
		
	}
	

}
