package com.cywc.mat6643.presenter;

//import org.opencv.highgui.Highgui;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;

import com.cywc.mat6643.MainActivity;
import com.cywc.mat6643.model.ImageModel;

public class LoadManager {
	private final MainActivity mainAct;
	
	public static int ERROR=-1;
	public static int GOOD= 0;
	
	public LoadManager(MainActivity act){
		mainAct=act;
	}
	
	public int loadImage(String path){
		BitmapFactory.Options options=new BitmapFactory.Options();
		options.inPreferredConfig=Bitmap.Config.ARGB_8888;
		try{
			ImageModel.bitmap=BitmapFactory.decodeFile(path,options);
		}catch(Exception e){
			return ERROR;
		}
		return GOOD;
	}
}
