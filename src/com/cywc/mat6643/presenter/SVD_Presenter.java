package com.cywc.mat6643.presenter;

//import org.opencv.android.BaseLoaderCallback;
//import org.opencv.android.LoaderCallbackInterface;
//import org.opencv.android.OpenCVLoader;
//import org.opencv.android.Utils;
//import org.opencv.core.Mat;

import Jama.Matrix;
import Jama.SingularValueDecomposition;

import com.cywc.mat6643.ImageProcessActivity;
import com.cywc.mat6643.ShowSVDActivity;
import com.cywc.mat6643.miscallenous.helper;
import com.cywc.mat6643.model.ImageModel;

public class SVD_Presenter {
	private ShowSVDActivity activity;
	private ImageModel model;
	private StringBuffer buf;
	
	public SVD_Presenter(ShowSVDActivity act,ImageModel aModel){
		activity=act;
		model=aModel;
		initialize();
	}
	
	public void initialize(){
		int imax=ImageModel.bitmap.getWidth();
		int jmax=ImageModel.bitmap.getHeight();
		double[][] greyscale= new double[imax][jmax];
		for(int i=0; i<imax; i++){
			for(int j=0; j<jmax; j++){
				greyscale[i][j]=(double)ImageModel.bitmap.getPixel(i, j);
			}
		}
		model.A=new Matrix(greyscale);
		activity.setValues(helper.MatrixToString(model.A));
	}
	
	public void computeSVD(){
		SingularValueDecomposition SVD=new SingularValueDecomposition(model.A);
		activity.appendHintBar("Here are the singular values");
		ImageModel.S=SVD.getSingularValues();
		activity.setValues(helper.ArrayToString(ImageModel.S));		
		ImageModel.U=SVD.getU();
		activity.appendHintBar("U is " + ImageModel.U.getRowDimension() + "X" + ImageModel.U.getColumnDimension());
		ImageModel.V=SVD.getV();
		activity.appendHintBar("V is " + ImageModel.V.getRowDimension() + "X" + ImageModel.V.getColumnDimension());
		activity.setTitleBar("SVD computed");
		ImageModel.SVD_computed=true;
	}
	
}
