package com.cywc.mat6643;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.cywc.mat6643.model.ImageModel;
import com.cywc.mat6643.presenter.ProcessManager;

public class ImageProcessActivity extends Activity {
	private ProcessManager manager;
	private int rank;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_image_process);
		hintbarAppend("\nthe height is " + ImageModel.bitmap.getHeight());
		hintbarAppend("\tthe width is " + ImageModel.bitmap.getWidth());
		getImageViewShow().setImageBitmap(ImageModel.bitmap);
		manager=new ProcessManager(new ImageModel(),this);
		//hide the keyboard until the edittext field is clicked.
		getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
		ImageModel.SVD_computed=false;
	}

	private void hintbarAppend(String append){
		TextView hintbar=getHintbar();
		String old=hintbar.getText().toString();
		hintbar.setText(old+append);
	}
	
	public void flipImage(View src){
		getHintbar().setText("Flip the image");
		manager.flipImage();
	}
	
	public void setImage(Bitmap aMap){
		getImageViewShow().setImageBitmap(aMap);
	}
	
	public void setHintbar(String str){
		getHintbar().setText(str);
	}
	
	private TextView getHintbar(){
		return (TextView)findViewById(R.id.process_hintbar);
	}
	
	private ImageView getImageViewShow(){
		return (ImageView)findViewById(R.id.shown_image);
	}
	
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.image_process, menu);
		return true;
	}

	
	public void drawOriginal(View src){
		getHintbar().setText("draw original graph");
		getImageViewShow().setImageBitmap(ImageModel.bitmap);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void SVD_Compress(View src){
		Intent intent=new Intent(this,ShowSVDActivity.class);
		startActivity(intent);
	}
	
	private EditText getSVDRankThreshold(){
		return (EditText)findViewById(R.id.SVD_rank_show);
	}
	
	public void sayInvalidSVDRank(){
		getHintbar().setText("Invalid rank for SVD");
	}
	
	public void returnToMain(View src){
		this.finish();
	}
	
	public void showSVDImages(View src){
		rank=getShowRank();
		if(rank<=0){
			getHintbar().setText("check the rank upper limit");
			return;
		}else if(!ImageModel.SVD_computed){
			getHintbar().setText("compute SVD first");
			return;
		}
		getHintbar().setText("show image from rank 1 to " + rank);
		InputMethodManager inputManager = (InputMethodManager)
                getSystemService(Context.INPUT_METHOD_SERVICE); 

		inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                   InputMethodManager.HIDE_NOT_ALWAYS);
		manager.showSVDSlides(rank);
		
	}
		
	
	public int getShowRank(){
		EditText edit=(EditText)findViewById(R.id.SVD_rank_show);
		int ans=-1;
		try{
			ans=Integer.parseInt(edit.getText().toString());
		}catch(Exception e){
			getHintbar().setText("invalid argument");
		}
		
		return ans;
	}
	
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_image_process,
					container, false);
			return rootView;
		}
	}

}
