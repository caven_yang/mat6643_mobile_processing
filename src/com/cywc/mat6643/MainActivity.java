package com.cywc.mat6643;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.ipaulpro.afilechooser.utils.FileUtils;
import com.cywc.mat6643.presenter.LoadManager;
import com.cywc.mat6643.model.ImageModel;
import com.cywc.mat6643.ImageProcessActivity;

public class MainActivity extends Activity {
	
	private static final String TAG="MainActivity";
	private static final int REQUEST_CODE=6384;
	private String inputPath="";
	private LoadManager manager;
	private ImageModel imageData;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		/*
		if (savedInstanceState == null) {
			getSupportFragmentManager().beginTransaction()
					.add(R.id.container, new PlaceholderFragment()).commit();
		}
		*/
		setContentView(R.layout.activity_main);
		
		getChooseFileButton().setOnClickListener(new OnClickListener(){
			@Override
			public void onClick(View v){
				showChooser();
			}
		});
		manager=new LoadManager(this);
		getNotes().setText("Project of CSE6643 Numerical Linear Algebra\n" +
				"Authors: Chen Yang & Chao Wu\n" +
				"This project aims at applying the methods on image processing" +
				" on Android phones.\n" +
				"So far only applicable for greyscale images to focus more on the "+
				"math part");
		//getHintBar().setText("You have chosen image file: " + inputPath);
		
	}
	
	private void showChooser(){
		Intent target=FileUtils.createGetContentIntent();
		Intent intent=Intent.createChooser(target, getString(R.string.chooser_title));
		try{
			startActivityForResult(intent, REQUEST_CODE);
		}catch(Exception e){
			//
		}
	}
	
	public String getPath(){
		return inputPath;
	}
	
	protected void onActivityResult(int requestCode, int resultCode,Intent data){
		switch(requestCode){
		case REQUEST_CODE:
			if(resultCode==RESULT_OK){
				if(data!=null){
					final Uri uri=data.getData();
					Log.i(TAG,"Uri="+uri.toString());
					try{
						final String path=FileUtils.getPath(this, uri);
						inputPath=path;
						//Toast.makeText(MainActivity.this,"File Selected: "+path, 
						//		Toast.LENGTH_LONG).show();
						inputPath=""+path;
						getHintBar().setText("You have chosen " + inputPath);
						
					}catch(Exception e){
						Log.e("FileSelectorTestActivity","File select error");
					}
				}
			}
			break;
		}
		super.onActivityResult(requestCode,resultCode,data);
	}
	
	private TextView getHintBar(){
		return (TextView)findViewById(R.id.main_hintbar);
	}
	
	private Button getChooseFileButton(){
		return (Button)findViewById(R.id.main_choose_file);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	private TextView getNotes(){
		return (TextView)findViewById(R.id.main_notes);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void onClickLoad(View src){
		if(inputPath.length()<5){
			getHintBar().setText("bad file path");
			return;
		}
		getHintBar().setText("Loading the image file");
		int loadResult=manager.loadImage(inputPath);
		if(loadResult==LoadManager.ERROR){
			getHintBar().setText("Error: cannot load the image");
		}else{
			getHintBar().setText("Image loaded");
			Intent intent=new Intent(this,ImageProcessActivity.class);
			startActivity(intent);
		}
	}

	
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_main, container,
					false);
			return rootView;
		}
	}

}
