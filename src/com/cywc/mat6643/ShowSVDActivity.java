package com.cywc.mat6643;

//import android.support.v7.app.ActionBarActivity;
//import android.support.v7.app.ActionBar;
//import org.opencv.android.LoaderCallbackInterface;
//import org.opencv.android.OpenCVLoader;
//import org.opencv.android.Utils;
//import org.opencv.android.BaseLoaderCallback;
//import org.opencv.core.Mat;

import Jama.Matrix;

import android.app.Activity;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.cywc.mat6643.miscallenous.helper;
import com.cywc.mat6643.model.ImageModel;
import com.cywc.mat6643.presenter.SVD_Presenter;

public class ShowSVDActivity extends Activity {
	private SVD_Presenter presenter;
	public static Matrix mat;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_svd);
		presenter=new SVD_Presenter(this,new ImageModel());
		presenter.computeSVD();
	}
	
	public void appendHintBar(String str){
		StringBuffer buf=new StringBuffer();
		buf.append(getHintBar().getText().toString());
		buf.append(str+"\n");
		getHintBar().setText(buf.toString());
	}
	

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.show_svd, menu);
		return true;
	}
	
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	public void setValues(String str){
		getValueBar().setText(str);
	}
	
	private TextView getHintBar(){
		return (TextView)findViewById(R.id.SVD_hint_bar);
	}
	
	public void setTitleBar(String str){
		getTitleBar().setText(str);
	}
	
	public void finishSVD(View src){
		this.finish();
	}
	
	private TextView getTitleBar(){
		return (TextView) findViewById(R.id.SVD_title_bar);
	}
	
	public void appendTitleBar(String str){
		String old=getTitleBar().getText().toString();
	}
	
	private TextView getValueBar(){
		return (TextView)findViewById(R.id.SVD_value_bar);
	}
	/**
	 * A placeholder fragment containing a simple view.
	 */
	public static class PlaceholderFragment extends Fragment {

		public PlaceholderFragment() {
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			View rootView = inflater.inflate(R.layout.fragment_show_svd,
					container, false);
			return rootView;
		}
	}

}
