package com.cywc.mat6643.miscallenous;

import Jama.Matrix;

public class helper {
	public static String ArrayToString(double[] array){
		StringBuffer buf=new StringBuffer();
		for(int i=0; i<array.length; i++){
			buf.append(array[i]+"  ");
		}
		return buf.toString();
	}
	
	
	public static String MatrixToString(Matrix mat){
		double[][] ans=mat.getArray();
		StringBuffer buf=new StringBuffer();
		
		for(int i=0; i<Math.min(20, ans.length); i++){
			for(int j=0; j<Math.min(20,ans.length); j++){
				buf.append(ans[i][j]+" ");
			}
			buf.append("\n");
		}
		
		
		return buf.toString();
	}
}
